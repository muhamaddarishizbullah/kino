<!DOCTYPE html>
<html class="no-js " lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistem Informasi Kino Cikembar.">
    <meta name="keywords" content="Sistem Informasi, Kino, Cikembar, CRM">
    <meta name="author" content="Muhammad Daris Hizbullah">
    <title>Kino</title>
    <link rel="icon" href="../agency/favicon.ico" type="image/x-icon"> <!-- Favicon-->

    <!-- Vendor CSS Files -->
    <link href="../assets/fonts/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../assets/plugin/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jsgrid@1.5.3/dist/jsgrid-theme.css">


    <link href="../assets/css/al.style.min.css" rel="stylesheet">
    <link href="../assets/css/al.agencyportfolio.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../main/assets/css/layout.a.min.css">

</head>

<body>