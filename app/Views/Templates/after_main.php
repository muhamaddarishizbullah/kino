    <!-- Modal: notifications -->
    <div class="modal fade" id="notificationsModal" tabindex="-1">
        <div class="modal-dialog modal-dialog-vertical modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header px-md-5">
                    <h5 class="modal-title">Notifications <span class="badge bg-danger ms-2">14</span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="bg-light border-bottom px-2 px-md-5 py-3">
                    <ul class="nav nav-pills nav-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#Noti-tab-Message" role="tab" aria-selected="true">Message</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#Noti-tab-Events" role="tab" aria-selected="false">Events</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#Noti-tab-Logs" role="tab">Logs</a>
                        </li>
                    </ul>
                </div>
                <div class="modal-body px-md-5 custom_scroll">
                    <div class="tab-content p-0">
                        <div class="tab-pane fade active show" id="Noti-tab-Message" role="tabpanel">
                            <ul class="list-unstyled list mb-0">
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <img class="avatar rounded-circle" src="../main/assets/images/xs/avatar1.jpg" alt="">
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Chris Fox</span> <small>2MIN</small></p>
                                            <span class="text-muted">changed an issue from "In Progress" to <span class="badge bg-success">Review</span></span>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <div class="avatar rounded-circle no-thumbnail">RH</div>
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Robert Hammer</span> <small>13MIN</small></p>
                                            <span class="text-muted">It is a long established fact that a reader
                                                will be distracted</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <img class="avatar rounded-circle" src="../main/assets/images/xs/avatar3.jpg" alt="">
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Orlando Lentz</span> <small>1HR</small></p>
                                            <span class="text-muted">There are many variations of passages</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <img class="avatar rounded-circle" src="../main/assets/images/xs/avatar4.jpg" alt="">
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Barbara Kelly</span> <small>1DAY</small></p>
                                            <span class="text-muted">Contrary to popular belief <span class="badge bg-danger">Code</span></span>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <img class="avatar rounded-circle" src="../main/assets/images/xs/avatar5.jpg" alt="">
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Robert Hammer</span> <small>13MIN</small></p>
                                            <span class="text-muted">making it over 2000 years old</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <img class="avatar rounded-circle" src="../main/assets/images/xs/avatar6.jpg" alt="">
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Orlando Lentz</span> <small>1HR</small></p>
                                            <span class="text-muted">There are many variations of passages</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <img class="avatar rounded-circle" src="../main/assets/images/xs/avatar7.jpg" alt="">
                                        <div class="flex-fill ms-3">
                                            <p class="d-flex justify-content-between mb-0 text-muted"><span class="fw-bold">Rose Rivera</span> <small class="">1DAY</small>
                                            </p>
                                            <span class="text-muted">The generated Lorem Ipsum</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="Noti-tab-Events" role="tabpanel">
                            <ul class="list-unstyled list mb-0">
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <div class="avatar rounded no-thumbnail"><i class="fa fa-info-circle fa-lg"></i></div>
                                        <div class="flex-fill ms-3">
                                            <p class="mb-0 text-muted">Campaign <strong class="text-primary">Holiday
                                                    Sale</strong> is nearly reach budget limit.</p>
                                            <small class="text-muted">10:00 AM Today</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <div class="avatar rounded no-thumbnail"><i class="fa fa-thumbs-up fa-lg"></i></div>
                                        <div class="flex-fill ms-3">
                                            <p class="mb-0 text-muted">Your New Campaign <strong class="text-primary">Holiday Sale</strong> is approved.</p>
                                            <small class="text-muted">11:30 AM Today</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <div class="avatar rounded no-thumbnail"><i class="fa fa-pie-chart fa-lg"></i></div>
                                        <div class="flex-fill ms-3">
                                            <p class="mb-0 text-muted">Website visits from Twitter is <strong class="text-danger">27% higher</strong> than last week.</p>
                                            <small class="text-muted">04:00 PM Today</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4 border-bottom">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <div class="avatar rounded no-thumbnail"><i class="fa fa-warning fa-lg"></i>
                                        </div>
                                        <div class="flex-fill ms-3">
                                            <p class="mb-0 text-muted"><strong class="text-warning">Error</strong>
                                                on website analytics configurations</p>
                                            <small class="text-muted">Yesterday</small>
                                        </div>
                                    </a>
                                </li>
                                <li class="py-4">
                                    <a href="javascript:void(0);" class="d-flex">
                                        <div class="avatar rounded no-thumbnail"><i class="fa fa-thumbs-up fa-lg"></i></div>
                                        <div class="flex-fill ms-3">
                                            <p class="mb-0 text-muted">Your New Campaign <strong class="text-primary">Holiday Sale</strong> is approved.</p>
                                            <small class="text-muted">11:30 AM Today</small>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="Noti-tab-Logs" role="tabpanel">
                            <h4>No Logs right now!</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Search -->
    <div class="modal fade" id="SearchModal" tabindex="-1">
        <div class="modal-dialog modal-dialog-vertical modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header bg-secondary border-bottom-0 px-3 px-md-5">
                    <h5 class="modal-title">Search</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body custom_scroll">
                    <div class="card-body-height py-4 px-2 px-md-4">
                        <form class="mb-3">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search...">
                                <button class="btn btn-outline-secondary" type="button"><span class="fa fa-search"></span> Search</button>
                            </div>
                        </form>

                        <small class="dropdown-header">Recent searches</small>
                        <div class="dropdown-item bg-transparent text-wrap my-2">
                            <span class="h4 me-1">
                                <a class="btn btn-sm btn-dark" href="#">Github <i class="fa fa-search ms-1"></i></a>
                            </span>
                            <span class="h4">
                                <a class="btn btn-sm btn-dark" href="#">Notification panel <i class="fa fa-search ms-1"></i></a>
                            </span>
                            <span class="h4">
                                <a class="btn btn-sm btn-dark" href="#">New project <i class="fa fa-search ms-1"></i></a>
                            </span>
                        </div>

                        <div class="dropdown-divider my-3"></div>

                        <small class="dropdown-header">Tutorials</small>
                        <a class="dropdown-item py-2" href="#">
                            <div class="d-flex align-items-center">
                                <span class="avatar sm no-thumbnail me-2"><i class="fa fa-github"></i></span>
                                <div class="text-truncate">
                                    <span>How to set up Github?</span>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item py-2" href="#">
                            <div class="d-flex align-items-center">
                                <span class="avatar sm no-thumbnail me-2"><i class="fa fa-paint-brush"></i></span>
                                <div class="text-truncate">
                                    <span>How to change theme color?</span>
                                </div>
                            </div>
                        </a>

                        <div class="dropdown-divider my-3"></div>

                        <small class="dropdown-header">Members</small>
                        <a class="dropdown-item py-2" href="#">
                            <div class="d-flex align-items-center">
                                <img class="avatar sm rounded-circle" src="../main/assets/images/xs/avatar1.jpg" alt="">
                                <div class="text-truncate ms-2">
                                    <span>Robert Hammer <i class="fa fa-check-circle text-primary" data-bs-toggle="tooltip" data-placement="top" title="" data-original-title="Top endorsed"></i></span>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item py-2" href="#">
                            <div class="d-flex align-items-center">
                                <img class="avatar sm rounded-circle" src="../main/assets/images/xs/avatar2.jpg" alt="">
                                <div class="text-truncate ms-2">
                                    <span>Orlando Lentz</span>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item py-2" href="#">
                            <div class="d-flex align-items-center">
                                <div class="avatar sm rounded-circle no-thumbnail">RH</div>
                                <div class="text-truncate ms-2">
                                    <span>Brian Swader</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Layout -->
    <div class="modal fade" id="LayoutModal" tabindex="-1">
        <div class="modal-dialog modal-lg modal-dialog-vertical modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Ready to build Layouts</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body custom_scroll">
                    <div class="mb-4">Customize your overview page layout. Choose the one that best fits your needs.</div>
                    <h5 class="mt-5 pb-2">Left sidebar with icon</h5>
                    <div class="row g-3">
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift border border-primary bg-primary text-light" href="index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-default.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Default</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="index-mini-sidebar.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-mini-sidebar.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Default + Menu Collapse</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-c/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-c.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Menu + Tab view</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-g/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-g.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Icon menu with Grid view</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-i/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-i.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Dual tone icon + menu list</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <h5 class="mt-5 pb-2">Header top menu</h5>
                    <div class="row g-3">
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-d/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-d.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header <span class="text-muted small">(Fluid)</span>
                                    </h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-d-container/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-d-container.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header <span class="text-muted small">(Container)</span></h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-d-sub-header/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-d-sub-header.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header + Sub menu <span class="text-muted small">(Fluid)</span></h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-d-sub-header-container/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-d-sub-header-container.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header + Submenu <span class="text-muted small">(Container)</span></h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-f/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-f.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header + Submenu, Overlay <span class="text-muted small">(Fluid)</span></h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-f-container/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-f-container.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header + Submenu, Overlay <span class="text-muted small">(Container)</span></h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-l/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-l.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Megamenu + Animation Overlay</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-q/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-q.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Header + Megamenu sticky</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <h5 class="mt-5 pb-2">Content Combinations</h5>
                    <div class="row g-3">
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-b/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-b.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Default</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-e/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-e.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Default</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-h/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-h.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Default</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-k/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-k.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Default</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-p/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-p.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Background BG</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <a class="card lift" href="layout-n/index.html">
                                <img class="card-img-top" src="../main/assets/images/layout/layout-n.svg" alt="" />
                                <div class="card-body text-center">
                                    <h6 class="card-title mb-0">Sidebar With Tab</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Setting -->
    <div class="modal fade" id="SettingsModal" tabindex="-1">
        <div class="modal-dialog modal-sm modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">AL-UI Setting</h5>
                </div>
                <div class="modal-body custom_scroll">
                    <!-- Settings: Font -->
                    <div class="setting-font">
                        <small class="card-title text-muted">Google font Settings</small>
                        <ul class="list-group font_setting mb-3 mt-1">
                            <li class="list-group-item py-1 px-2">
                                <div class="form-check mb-0">
                                    <input class="form-check-input" type="radio" name="font" id="font-opensans" value="font-opensans" checked="">
                                    <label class="form-check-label" for="font-opensans">Open Sans Google Font</label>
                                </div>
                            </li>
                            <li class="list-group-item py-1 px-2">
                                <div class="form-check mb-0">
                                    <input class="form-check-input" type="radio" name="font" id="font-quicksand" value="font-quicksand">
                                    <label class="form-check-label" for="font-quicksand">Quicksand Google Font</label>
                                </div>
                            </li>
                            <li class="list-group-item py-1 px-2">
                                <div class="form-check mb-0">
                                    <input class="form-check-input" type="radio" name="font" id="font-nunito" value="font-nunito">
                                    <label class="form-check-label" for="font-nunito">Nunito Google Font</label>
                                </div>
                            </li>
                            <li class="list-group-item py-1 px-2">
                                <div class="form-check mb-0">
                                    <input class="form-check-input" type="radio" name="font" id="font-Raleway" value="font-raleway">
                                    <label class="form-check-label" for="font-Raleway">Raleway Google Font</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- Settings: Color -->
                    <div class="setting-theme">
                        <small class="card-title text-muted">Theme Color Settings</small>
                        <ul class="list-unstyled d-flex justify-content-between choose-skin mb-2 mt-1">
                            <li data-theme="indigo">
                                <div class="indigo"></div>
                            </li>
                            <li data-theme="blue" class="active">
                                <div class="blue"></div>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                            </li>
                            <li data-theme="blush">
                                <div class="blush"></div>
                            </li>
                            <li data-theme="red">
                                <div class="red"></div>
                            </li>
                            <li data-theme="dynamic">
                                <div class="dynamic"><i class="fa fa-paint-brush"></i></div>
                            </li>
                        </ul>
                        <div class="form-check form-switch gradient-switch mb-1">
                            <input class="form-check-input" type="checkbox" id="CheckGradient">
                            <label class="form-check-label" for="CheckGradient">Enable Gradient! ( Sidebar )</label>
                        </div>
                    </div>
                    <!-- Settings: bg image -->
                    <div class="setting-img mb-3">
                        <div class="form-check form-switch imagebg-switch mb-1">
                            <input class="form-check-input" type="checkbox" id="CheckImage">
                            <label class="form-check-label" for="CheckImage">Set Background Image (Sidebar)</label>
                        </div>
                        <div class="bg-images">
                            <ul class="list-unstyled d-flex justify-content-between">
                                <li class="sidebar-img-1 sidebar-img-active"><a class="rounded sidebar-img" id="img-1" href="#"><img src="../main/assets/images/sidebar-bg/sidebar-1.jpg" alt="" /></a></li>
                                <li class="sidebar-img-2"><a class="rounded sidebar-img" id="img-2" href="#"><img src="../main/assets/images/sidebar-bg/sidebar-2.jpg" alt="" /></a></li>
                                <li class="sidebar-img-3"><a class="rounded sidebar-img" id="img-3" href="#"><img src="../main/assets/images/sidebar-bg/sidebar-3.jpg" alt="" /></a></li>
                                <li class="sidebar-img-4"><a class="rounded sidebar-img" id="img-4" href="#"><img src="../main/assets/images/sidebar-bg/sidebar-4.jpg" alt="" /></a></li>
                                <li class="sidebar-img-5"><a class="rounded sidebar-img" id="img-5" href="#"><img src="../main/assets/images/sidebar-bg/sidebar-5.jpg" alt="" /></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Settings: Theme dynamics -->
                    <div class="dt-setting">
                        <small class="card-title text-muted">Dynamic Color Settings</small>
                        <ul class="list-group list-unstyled mb-3 mt-1">
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label>Primary Color</label>
                                <button id="primaryColorPicker" class="btn bg-primary avatar xs border-0 rounded-0"></button>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label>Secondary Color</label>
                                <button id="secondaryColorPicker" class="btn bg-secondary avatar xs border-0 rounded-0"></button>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label class="text-muted small">Chart Color 1</label>
                                <button id="chartColorPicker1" class="btn chart-color1 avatar xs border-0 rounded-0"></button>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label class="text-muted small">Chart Color 2</label>
                                <button id="chartColorPicker2" class="btn chart-color2 avatar xs border-0 rounded-0"></button>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label class="text-muted small">Chart Color 3</label>
                                <button id="chartColorPicker3" class="btn chart-color3 avatar xs border-0 rounded-0"></button>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label class="text-muted small">Chart Color 4</label>
                                <button id="chartColorPicker4" class="btn chart-color4 avatar xs border-0 rounded-0"></button>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center py-1 px-2">
                                <label class="text-muted small">Chart Color 5</label>
                                <button id="chartColorPicker5" class="btn chart-color5 avatar xs border-0 rounded-0"></button>
                            </li>
                        </ul>
                    </div>
                    <!-- Settings: Light/dark -->
                    <div class="setting-mode">
                        <small class="card-title text-muted">Light/Dark & Contrast Layout</small>
                        <ul class="list-group list-unstyled mb-0 mt-1">
                            <li class="list-group-item d-flex align-items-center py-1 px-2">
                                <div class="form-check form-switch theme-switch mb-0">
                                    <input class="form-check-input" type="checkbox" id="theme-switch">
                                    <label class="form-check-label" for="theme-switch">Enable Dark Mode!</label>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center py-1 px-2">
                                <div class="form-check form-switch theme-high-contrast mb-0">
                                    <input class="form-check-input" type="checkbox" id="theme-high-contrast">
                                    <label class="form-check-label" for="theme-high-contrast">Enable High Contrast</label>
                                </div>
                            </li>
                            <li class="list-group-item d-flex align-items-center py-1 px-2">
                                <div class="form-check form-switch theme-rtl mb-0">
                                    <input class="form-check-input" type="checkbox" id="theme-rtl">
                                    <label class="form-check-label" for="theme-rtl">Enable RTL Mode!</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-start text-center">
                    <button type="button" class="btn flex-fill btn-primary lift">Save Changes</button>
                    <button type="button" class="btn flex-fill btn-white border lift" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>