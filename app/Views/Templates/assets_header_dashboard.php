<!DOCTYPE html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistem Informasi Kino Cikembar.">
    <meta name="keywords" content="Sistem Informasi, Kino, Cikembar, CRM">
    <meta name="author" content="Muhammad Daris Hizbullah">
    <title>Kino</title>
    <link rel="icon" href="../agency/favicon.ico" type="image/x-icon"> <!-- Favicon-->

    <!-- project css file  -->
    <link rel="stylesheet" href="../main/assets/css/al.style.min.css">

    <!-- plugin css file  -->

    <!-- project layout css file -->
    <link rel="stylesheet" href="../main/assets/css/layout.a.min.css">
    <!-- Bootstrap core CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> -->
    <link href="../plugin/datatables.min.css" rel="stylesheet">
</head>

<body>