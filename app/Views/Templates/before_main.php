    <!-- Navigation -->
    <div class="navigation navbar navbar-light justify-content-center px-3 px-lg-2 py-2 py-md-3 border-end">

        <!-- Brand -->
        <a href="<?= base_url('/') ?>" class="mb-0 mb-lg-3 brand-icon">
            <img src="../agency/kino.png" alt="Kino Icon" width="30">
        </a>

        <!-- Menu: icon -->
        <ul class="nav navbar-nav flex-row flex-sm-column flex-grow-1 justify-content-start py-2 py-lg-0">

            <li class="nav-item d-none d-sm-block flex-grow-1">
                <!-- <a class="nav-link p-2 p-lg-3" href="calendar.html"><i class="fa fa-calendar"></i></a> -->
            </li>

            <!-- Menu collapse -->
            <li class="nav-item d-none d-sm-block"><a class="nav-link p-2 p-lg-3" href="<?= base_url('/logout') ?>" title="Menu collapse"><i class="fa fa-power-off"></i></a></li>

        </ul>

    </div>

    <!-- sidebar -->
    <div class="sidebar px-3 py-2 py-md-3">
        <div class="d-flex flex-column h-100">
            <div class="d-flex align-items-center mb-4 mt-2">
                <h4 class="sidebar-title mb-0 flex-grow-1">IS<span>Kino</span></h4>

            </div>
            <form class="mb-2 mt-1">
                <div class="input-group">
                    <input type="text" class="form-control border-0" placeholder="Search...">
                </div>
            </form>

            <!-- Menu: main ul -->
            <ul class="menu-list flex-grow-1">
                <li><a class="m-link active" href="<?= base_url('/scale-up-validation') ?>"><i class="fa fa-flag"></i><span>Scale Up & Validation</span></a></li>

                <li><a class="m-link" href="<?= base_url('/landing-page') ?>"><i class="fa fa-star"></i><span>Landing Page</span></a></li>
            </ul>

            <!-- Menu: menu collepce btn -->
            <button type="button" class="btn btn-link sidebar-mini-btn text-light">
                <span><i class="fa fa-arrow-left"></i></span>
            </button>
        </div>
    </div>