<div class="col-lg-6 d-none d-lg-flex justify-content-center align-items-center rounded-lg auth-h100">
    <div style="max-width: 25rem;">
        <div class="text-center mb-5">
            <img src="../agency/kino.png" alt="Kino Icon" width="70">
        </div>

        <div class="mb-5">
            <h2 class="color-900">Build digital products with:</h2>
        </div>

        <!-- List Checked -->
        <ul class="mb-5">
            <li class="mb-4">
                <span class="d-block fw-bold mb-1">All-in-one tool</span>
                <span class="text-muted">Build, run, and scale your apps - end to end</span>
            </li>

            <li>
                <span class="d-block fw-bold">Easily add &amp; manage your services</span>
                <span class="text-muted">It brings together your tasks, projects, timelines, files and more</span>
            </li>
        </ul>

        <div class="ms-4 ps-2">
            <a href="#" class="me-2 text-muted"><i class="fa fa-facebook-square fa-lg"></i></a>
            <a href="#" class="me-2 text-muted"><i class="fa fa-github-square fa-lg"></i></a>
            <a href="#" class="me-2 text-muted"><i class="fa fa-linkedin-square fa-lg"></i></a>
            <a href="#" class="me-2 text-muted"><i class="fa fa-twitter-square fa-lg"></i></a>
        </div>
    </div>
</div>