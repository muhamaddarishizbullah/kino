<?= view('App\Views\Templates\assets_header') ?>
<div id="layout-a" class="theme-blue">

    <!-- main body area -->
    <div class="main auth-div p-2 py-3 p-xl-5">

        <!-- Body: Body -->
        <div class="body d-flex p-0 p-xl-5">
            <div class="container-fluid">

                <div class="row g-0">
                    <?= view('App\Views\Templates\left_auth') ?>

                    <div class="col-lg-6 d-flex justify-content-center align-items-center border-0 rounded-lg auth-h100">
                        <div class="w-100 p-4 p-md-5 card border-0" style="max-width: 32rem;">
                            <!-- Form -->
                            <?= view('App\Views\Auth\_message_block') ?>
                            <form class="row g-1 p-0 p-md-4" action="<?= url_to('login') ?>" method="post">
                                <?= csrf_field() ?>
                                <div class="col-12 text-center mb-5">
                                    <h1>Sign in</h1>
                                    <span>Free access to our dashboard.</span>
                                </div>
                                <?php if ($config->validFields === ['email']) : ?>
                                    <div class="col-12">
                                        <div class="mb-2">
                                            <label class="form-label">Email</label>
                                            <input type="email" name="login" class="form-control form-control-lg <?php if (session('errors.login')) : ?>is-invalid<?php endif ?>" placeholder="name@example.com">
                                        </div>
                                        <div class="invalid-feedback">
                                            <?= session('errors.login') ?>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="col-12">
                                        <div class="mb-2">
                                            <label class="form-label">Email or username</label>
                                            <input type="text" name="login" class="form-control form-control-lg <?php if (session('errors.login')) : ?>is-invalid<?php endif ?>" placeholder="name@example.com or JohnParker47">
                                        </div>
                                        <div class="invalid-feedback">
                                            <?= session('errors.login') ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-12">
                                    <div class="mb-2">
                                        <div class="form-label">
                                            <span class="d-flex justify-content-between align-items-center">
                                                Password
                                                <?php if ($config->activeResetter) : ?>
                                                    <a class="text-primary" href="<?= url_to('forgot') ?>">Forgot Password?</a>
                                                <?php endif; ?>
                                            </span>
                                        </div>
                                        <input type="password" name="password" class="form-control form-control-lg  <?php if (session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?= lang('Auth.password') ?>" placeholder="***************">
                                        <div class="invalid-feedback">
                                            <?= session('errors.password') ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($config->allowRemembering) : ?>
                                    <div class="col-12">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" name="remember" <?php if (old('remember')) : ?> checked <?php endif ?>>
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-12 text-center mt-4">
                                    <button type="submit" class="btn btn-lg btn-block btn-dark lift text-uppercase">SIGN IN</button>
                                </div>
                                <?php if ($config->allowRegistration) : ?>
                                    <div class="col-12 text-center mt-4">
                                        <span class="text-muted">Don't have an account yet? <a href="<?= base_url('/register') ?>">Sign up here</a></span>
                                    </div>
                                <?php endif; ?>
                            </form>
                            <!-- End Form -->
                        </div>
                    </div>
                </div> <!-- End Row -->

            </div>
        </div>

        <div class="animate_lines">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>

    </div>



</div>

<?= view('App\Views\Templates\assets_footer') ?>