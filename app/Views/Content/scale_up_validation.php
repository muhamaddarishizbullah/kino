<?= view('App\Views\Templates\assets_header_dashboard') ?>

<div id="layout-a" class="theme-blue">

    <?= view('App\Views\Templates\before_main') ?>

    <!-- main body area -->
    <div class="main px-md-3">

        <!-- Body: Header -->
        <div class="body-header border-bottom d-flex py-3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col">
                        <small class="text-muted">Welcome back</small>
                        <h1 class="h4 mt-1">Scale Up & Validation</h1>
                    </div>
                    <div class="col-auto">
                        <a href="#" title="Download" target="_blank" class="btn btn-white border lift">Download</a>
                        <button type="button" class="btn btn-dark lift">Generate Report</button>
                    </div>
                </div> <!-- .row end -->
            </div>
        </div>

        <!-- Body: Body -->
        <div class="body d-flex py-lg-4 py-3">
            <div class="container">

                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="card p-4 mb-4">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>No</th>
                                            <th>Date</th>
                                            <th>Product</th>
                                            <th>Varian</th>
                                            <th>No Batch</th>
                                            <th>Batch Size</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Validator/Formulator</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div> <!-- .card end -->
                        <!-- <div class="card mb-4 border-0">
                            <div class="card-header py-3 d-flex flex-wrap  justify-content-between align-items-center bg-transparent border-bottom-0">
                                <div>
                                    <h6 class="m-0">Transaction History</h6>
                                </div>
                                <div class="dropdown">
                                    <button class="btn btn-sm btn-link text-muted d-none d-sm-inline-block" type="button"><i class="fa fa-download"></i></button>
                                    <button class="btn btn-sm btn-link text-muted d-none d-sm-inline-block" type="button"><i class="fa fa-external-link"></i></button>
                                    <button class="btn btn-sm btn-link text-muted dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"></button>
                                    <ul class="dropdown-menu dropdown-animation dropdown-menu-end shadow border-0">
                                        <li><a class="dropdown-item" href="#">Action<i class="fa fa-arrow-right"></i></a></li>
                                        <li><a class="dropdown-item" href="#">Another action<i class="fa fa-arrow-right"></i></a></li>
                                        <li><a class="dropdown-item" href="#">Something else here<i class="fa fa-arrow-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="myTransaction" class="table display dataTable table-hover align-middle" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Order No.</th>
                                            <th>Customer</th>
                                            <th>Date</th>
                                            <th>Ref</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="sorting_1">#B1245</td>
                                            <td>
                                                <a href="#" class="d-flex align-items-center">
                                                    <div class="me-2">
                                                        <img class="rounded-circle avatar sm" src="../assets/images/xs/avatar1.jpg" alt="avatar" title="">
                                                    </div>
                                                    <div class="media-body">
                                                        Chris Fox
                                                    </div>
                                                </a>
                                            </td>
                                            <td>02/11/2020</td>
                                            <td>SUB-2309232</td>
                                            <td><span class="badge bg-success">Paid</span></td>
                                            <td>$1,015</td>
                                            <td class="dt-body-right">
                                                <div class="dropdown">
                                                    <button class="btn btn-link dropdown-toggle after-none" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                                    <ul class="dropdown-menu border-0 shadow p-3">
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Another action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Something else here</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="sorting_1">#B4518</td>
                                            <td>
                                                <a href="#" class="d-flex align-items-center">
                                                    <div class="me-2">
                                                        <img class="rounded-circle avatar sm" src="../assets/images/xs/avatar2.jpg" alt="avatar" title="">
                                                    </div>
                                                    <div class="media-body">
                                                        Barbara Kelly
                                                    </div>
                                                </a>
                                            </td>
                                            <td>05/11/2020</td>
                                            <td>SUB-2304593</td>
                                            <td><span class="badge bg-success">Paid</span></td>
                                            <td>$8,015</td>
                                            <td class="dt-body-right">
                                                <div class="dropdown">
                                                    <button class="btn btn-link dropdown-toggle after-none" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                                    <ul class="dropdown-menu border-0 shadow p-3">
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Another action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Something else here</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="sorting_1">#B7878</td>
                                            <td>
                                                <a href="#" class="d-flex align-items-center">
                                                    <div class="me-2">
                                                        <img class="rounded-circle avatar sm" src="../assets/images/xs/avatar3.jpg" alt="avatar" title="">
                                                    </div>
                                                    <div class="media-body">
                                                        Brian Swader
                                                    </div>
                                                </a>
                                            </td>
                                            <td>11/11/2020</td>
                                            <td>SUB-2304093</td>
                                            <td><span class="badge bg-success">Paid</span></td>
                                            <td>$12,580</td>
                                            <td class="dt-body-right">
                                                <div class="dropdown">
                                                    <button class="btn btn-link dropdown-toggle after-none" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                                    <ul class="dropdown-menu border-0 shadow p-3">
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Another action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Something else here</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="sorting_1">#B2583</td>
                                            <td>
                                                <a href="#" class="d-flex align-items-center">
                                                    <div class="me-2">
                                                        <img class="rounded-circle avatar sm" src="../assets/images/xs/avatar4.jpg" alt="avatar" title="">
                                                    </div>
                                                    <div class="media-body">
                                                        Rose Rivera
                                                    </div>
                                                </a>
                                            </td>
                                            <td>17/11/2020</td>
                                            <td>SUB-2594093</td>
                                            <td><span class="badge bg-danger">Canceled</span></td>
                                            <td>$22,180</td>
                                            <td class="dt-body-right">
                                                <div class="dropdown">
                                                    <button class="btn btn-link dropdown-toggle after-none" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                                    <ul class="dropdown-menu border-0 shadow p-3">
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Another action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Something else here</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="sorting_1">#B1010</td>
                                            <td>
                                                <a href="#" class="d-flex align-items-center">
                                                    <div class="me-2">
                                                        <img class="rounded-circle avatar sm" src="../assets/images/xs/avatar5.jpg" alt="avatar" title="">
                                                    </div>
                                                    <div class="media-body">
                                                        Frank Camly
                                                    </div>
                                                </a>
                                            </td>
                                            <td>17/11/2020</td>
                                            <td>SUB-2594093</td>
                                            <td><span class="badge bg-warning">Due</span></td>
                                            <td>$13,550</td>
                                            <td class="dt-body-right">
                                                <div class="dropdown">
                                                    <button class="btn btn-link dropdown-toggle after-none" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></button>
                                                    <ul class="dropdown-menu border-0 shadow p-3">
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Another action</a></li>
                                                        <li><a class="dropdown-item py-2 rounded" href="#">Something else here</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> .card end -->
                    </div>
                </div> <!-- .row end -->

            </div>
        </div>

        <!-- Body: Footer -->
        <div class="body-footer">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card p-3 mb-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col">
                                    <p class="mb-0">© Kino Indonesia. <span class="d-none d-sm-inline-block">
                                            <script>
                                                document.write(/\d{4}/.exec(Date())[0])
                                            </script>.
                                        </span></p>
                                </div>
                                <div class="col-auto">
                                    <div class="d-flex justify-content-end">
                                        <!-- List Dot -->
                                        <!-- <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a class="list-separator-link" href="https://www.thememakker.com/about/">About</a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="list-separator-link" href="https://www.thememakker.com/hire-us/">Hire us</a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="list-separator-link" href="https://www.thememakker.com/all-templates/">Template</a>
                                            </li>

                                            <li class="list-inline-item">
                                                <a class="list-separator-link" href="https://themeforest.net/licenses/standard" target="_blank">License</a>
                                            </li>
                                        </ul> -->
                                        <!-- End List Dot -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?= view('App\Views\Templates\after_main') ?>

</div>
<?= $this->section('script') ?>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: '<?= base_url('/scale-up-validation/data') ?>',
            order: [],
            dom: 'Bfrtip',
            buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columnDefs": [{
                    "width": "200px",
                    "targets": 2
                }, // Lebar maksimum untuk kolom pertama
                {
                    "width": "150px",
                    "targets": 1
                }, // Lebar maksimum untuk kolom kedua
                // ... tambahkan konfigurasi lebar maksimum untuk kolom lainnya
            ],
            columns: [{
                    data: 'no',
                    orderable: false
                }, {
                    data: 'date'
                },
                {
                    data: 'product'
                },
                {
                    data: 'varian'
                },
                {
                    data: 'no_batch'
                },
                {
                    data: 'batch_size'
                },
                {
                    data: 'title'
                },
                {
                    data: 'description'
                },
                {
                    data: 'user'
                },
                {
                    data: 'action'
                }
            ]
        });
    });
</script>
<?= $this->endSection() ?>
<?= view('App\Views\Templates\assets_footer_tables') ?>