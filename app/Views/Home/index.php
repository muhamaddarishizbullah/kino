<?= view('App\Views\Templates\assets_header_dashboard') ?>

<div id="layout-a" class="theme-blue">

    <?= view('App\Views\Templates\before_main') ?>

    <!-- main body area -->
    <div class="main px-md-3">

        <!-- Body: Header -->
        <div class="body-header border-bottom d-flex py-3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col">
                        <small class="text-muted">Welcome back</small>
                        <h1 class="h4 mt-1">Scale Up & Validation</h1>
                    </div>
                    <div class="col-auto">
                        <!-- <a href="#" title="Download" target="_blank" class="btn btn-white border lift">Download</a>
                        <button type="button" class="btn btn-dark lift">Generate Report</button> -->
                    </div>
                </div> <!-- .row end -->
            </div>
        </div>

        <!-- Body: Body -->
        <div class="body d-flex py-lg-4 py-3">
            <div class="container">

                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="card p-4 mb-4">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>No</th>
                                            <th>Date</th>
                                            <th>Product</th>
                                            <th>Varian</th>
                                            <th>No Batch</th>
                                            <th>Batch Size</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Validator/Formulator</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div> <!-- .card end -->

                    </div>
                </div> <!-- .row end -->

            </div>
        </div>

        <!-- Body: Footer -->
        <div class="body-footer">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card p-3 mb-3">
                            <div class="row justify-content-between align-items-center">
                                <div class="col">
                                    <p class="mb-0">© Kino Indonesia. <span class="d-none d-sm-inline-block">
                                            <script>
                                                document.write(/\d{4}/.exec(Date())[0])
                                            </script>.
                                        </span></p>
                                </div>
                                <div class="col-auto">
                                    <div class="d-flex justify-content-end">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>
<?= $this->section('script') ?>
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: '<?= base_url('/scale-up-validation/data') ?>',
            order: [],
            dom: 'Bfrtip',
            buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
            "columnDefs": [{
                    "width": "200px",
                    "targets": 2
                }, // Lebar maksimum untuk kolom pertama
                {
                    "width": "150px",
                    "targets": 1
                }, // Lebar maksimum untuk kolom kedua
                // ... tambahkan konfigurasi lebar maksimum untuk kolom lainnya
            ],
            columns: [{
                    data: 'no',
                    orderable: false
                }, {
                    data: 'date'
                },
                {
                    data: 'product'
                },
                {
                    data: 'varian'
                },
                {
                    data: 'no_batch'
                },
                {
                    data: 'batch_size'
                },
                {
                    data: 'title'
                },
                {
                    data: 'description'
                },
                {
                    data: 'user'
                },
                {
                    data: 'action'
                }
            ]
        });
    });
</script>
<?= $this->endSection() ?>
<?= view('App\Views\Templates\assets_footer') ?>