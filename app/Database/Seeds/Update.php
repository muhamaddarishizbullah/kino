<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Update extends Seeder
{
    public function run()
    {
        $this->db->disableForeignKeyChecks();
        //create table scaleup validasi
        $this->call('DScaleUpValidation001');
        //input data awal
        $this->call('DScaleUpValidation002');
        $this->db->enableForeignKeyChecks();
    }
}
