<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class DScaleUpValidation001 extends Seeder
{
    public function run()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `d_scale_up_validation`  (
            `id` int NOT NULL AUTO_INCREMENT,
            `date` varchar(20) NOT NULL,
            `product` varchar(255) NOT NULL,
            `varian` varchar(255) NULL DEFAULT NULL,
            `no_batch` varchar(20)NOT NULL,
            `batch_size` varchar(20) NOT NULL,
            `title` varchar(50) NOT NULL,
            `description` text NOT NULL,
            `user` varchar(50) NULL,
            PRIMARY KEY (`id`) USING BTREE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
    }
}
