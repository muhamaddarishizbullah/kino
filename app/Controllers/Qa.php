<?php

namespace App\Controllers;

use Hermawan\DataTables\DataTable;

class Qa extends BaseController
{

    public function __construct()
    {
    }

    public function scaleUpValidation()
    {
        return view('Content/scale_up_validation');
    }

    public function scaleUpValidationData()
    {
        $db = db_connect();
        $builder = $db->table('d_scale_up_validation')->select('id, date, product, varian, no_batch, batch_size, title, description, user');

        return DataTable::of($builder)->add('action', function ($row) {
            return '
                   <button type="button" class="btn btn-warning btn-sm" data="' . $row->id . '">Edit</button>
                   <button type="button" class="btn btn-danger btn-sm" data="' . $row->id . '">Delete</button>
               ';
        })->addNumbering('no')->toJson(true);
    }

    public function landingPage()
    {
        return view('Home/indexb');
    }
}
